<?php

use Mpdf\Mpdf;
use Entity\OrdemDeServico;

class RelatoriosController extends CI_Controller
{
    private $mpdf;

    public function __construct()
    {
        parent::__construct();
        if ( !$this->session->usuario ) {
            redirect('/login');
        }
        $this->twig->addGlobal('session', $this->session);

        $this->mpdf = new Mpdf();
    }

    public function index() {
        $this->twig->display('app/relatorios/index');
    }

    public function gerarPDF() {
        $items       = $this->input->post('status');
        $dataInicial = null;
        $dataFinal   = null;
        $os          = null;

        if ( $dataInicial && !$dataFinal ) {
            $os = $this->doctrine->em->getRepository(OrdemDeServico::class)->findBy(
                ['status' => $items],
                ['dataIncial' >= $dataInicial ]
            );
        }

        if ( $dataFinal && !$dataInicial ) {
            $os = $this->doctrine->em->getRepository(OrdemDeServico::class)->findBy(
                ['status' => $items],
                ['dataFinal' <= $dataFinal ]
            );
        }

        if ( $dataFinal && $dataInicial ) {
            $os = $this->doctrine->em->getRepository(OrdemDeServico::class)->findBy(
                ['status' => $items],
                ['dataInicial' => $data]
            );
        }


        $os = $this->doctrine->em->getRepository(OrdemDeServico::class)->findBy(['status' => $items]);

        $this->mpdf->writeHTML( $this->twig->render('app/relatorios/pdf/relatorio-os', ['ordens' => $os]) );
        $this->mpdf->Output();
    }
}